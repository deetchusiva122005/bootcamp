def to_check_if_ascending(word: str) -> bool:
    char = word[0]
    for i in word:
        if char > i:
            return False
        char = i
    return 'A'


def to_check_if_descending(word: str) -> bool:
    char = word[0]
    for i in word:
        if char < i:
            return False
        char = i
    return 'D'


def to_check_if_peak(word: str) -> bool:
    char = word[0]
    peak = max(word)
    for i in range(1, word.index(peak) + 1):
        if char > word[i]:
            return False
        char = word[i]
    for i in range(word.index(peak) + 1, len(word)):
        if char < word[i]:
            return False
        char = word[i]
    return 'P'


def to_check_if_valley(word: str) -> bool:
    char = word[0]
    peak = min(word)
    for i in range(1, word.index(peak) + 1):
        if char < word[i]:
            return False
        char = word[i]
    for i in range(word.index(peak) + 1, len(word)):
        if char > word[i]:
            return False
        char = word[i]
    return 'V'


print(to_check_if_ascending("123"))
print(to_check_if_descending("1234"))
print(to_check_if_peak("1234321"))
print(to_check_if_valley("2313222"))
