import math
def prime_numbers( num : int) :
    primes=[]
    for i in  range(2,num+1):
        if i<math.sqrt(num):
            if i==2 or  i==3:
                primes.append(i)   
            elif i%2 !=0 and i%3 !=0:
                primes.append(i)
        else:
            if all((i%j)!=0 for j in range(2,10)):
                primes.append(i) 
    return primes


print(prime_numbers(50))

def  generate_primes(limit : int):
    primes = [2,3,5]
    for n in range(6,limit,6):
        for step in [1,5]:
            if is_prime(n+step):
                primes.append(n + step)
    return primes

def is_prime(n : int) -> bool:
        factor = 5
        while factor * factor <=n:
            if n%factor == 0:
                return False
            factor += 2
        return  True

print(generate_primes(100))

def  generate_primes(limit : int):
    def is_prime(n : int) -> bool:
        factor = 7
        while factor * factor <=n:
            if n%factor == 0:
                return False
            factor += 2
        return  True
    
    primes = [2,3,5,7,11,13,17,19,23,29]

    for n in range(30,limit,30):
        for step in [1,7,11,13,17,19,23,29]:
            if is_prime(n+step):
                primes.append(n + step)
    return primes

print(generate_primes(100))

#saturday
#sum of digits should always be a single digit
def sum_digits(num : int) -> int:
    sum=0
    while num>0:
        n = num % 10
        sum+=n
        num//=10
    if sum>9:
        return sum_digits(sum)
    return sum
print(sum_digits(9786346233))

def sum_digits(num):
    d=num%9
    return 9 if d==0 else d
print(sum_digits(9786346233))
#if you are asked to print a  new number, the order must have been changed.
# so try alter way"""
def new_digit(num : int) -> int:
    n = str(num)
    number =""
    for i in range(len(n)):
        if i % 2 == 1:
        #for i in n[-1::-2] :
            number += str((2*int(n[i])))
        else:    
        #for i in n[-2::-2] :
            number += str(int(n[i]))
    return int(number)


def sum_digit(num : int):
    new = new_digit(num)
    Sum = 0
    while new>0:
        n = new % 10
        Sum += n
        new //= 10
    return  checkdigit(Sum)


def checkdigit(total : int) -> int:
    s = 10 - total % 10
    return s


print(sum)   
print(sum_digit(7389192797))



