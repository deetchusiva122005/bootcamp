import sys
LINEWIDTH = 40
STAR = "#"
DASH = "_"
LF ="\n"
START , REPEAT , LAST = STAR , DASH + DASH, "\b" + STAR

def pattern(size : int):
    return [line(i) for i in range(size)]


def line(line_num : int) -> str:
    return start(line_num) + repeat(line_num) + last(line_num)

def start(n: int) -> str :
    return START

def last(n: int) -> str:
    return LAST

def repeat(n: int) -> str:
    return REPEAT * n

def make_pyramind(size : int) -> str:
    return LF.join([line.center(LINEWIDTH) for line in pattern(size)])

def make_right(size : int) -> str :
    return LF.join([line.rjust(LINEWIDTH) for line in pattern(size)])

def make_arrow(size : int) -> str:
    base = pattern(size)
    arrow = base * base[::1][1:]
    return LF.join([line.rjust(LINEWIDTH) for line in arrow]) 

#def arg in sys.argv[1:]:
print(make_pyramind(int(4)))
