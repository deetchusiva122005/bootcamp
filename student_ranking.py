from itertools import combinations as nCr
COMMENT = "#"
LT , GT , NE = "<" , ">" , "#"

def load_data(filename: str) -> dict[str , list[int]]:
    with open(filename) as f:
        for line in f:
            if line[0] != COMMENT:
                name , *raw_marks = line.split()
                data[name] = [int(r) for r in raw_marks]
    return data

def relation(a_student: list[int], b_student: list[int]) -> str:
    if all( a < b for (a, b) in zip(a_student , b_student)):
        return LT
    elif all(a > b for (a,b) in zip(a_student , b_student)):
        return GT
    else:
        return NE
    
def all_relations(filename: str) -> list[str]:
    data = load_data(filename)
    pairs = nCr(data.keys(), 2)
    #relations = set()
    #for pair in pairs:
    #   rel = relation(data[pair[0]] , data[pair[1]])
    #   relations.add(f'{pair[0]}')

    return {f'{pair[0]}{relation(data[pair[0]],data[pair[1]])}{pair[1]}' for pair in pairs}

print(comparables("data.txt"))
