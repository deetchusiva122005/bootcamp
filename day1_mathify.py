def split_terms(poly_eqn : str) -> str:
    split_eqn = poly_eqn.replace('-','+-')
    terms = split_eqn.split("+")
    return terms

def new_poly_eqn_terms(terms : str) -> str:
    new_poly_terms = list(reversed(terms))
    return new_poly_terms

def new_poly_eqn(poly_eqn : str) -> str:
    terms = split_terms(poly_eqn)
    new_terms = new_poly_eqn_terms(terms)
    eqn = '+'.join(new_terms)
    new_eqn = eqn.replace('+-' , '-')
    new_poly_equation = ''.join(new_eqn)
    return new_poly_equation


print(new_poly_eqn("2x^4+3x-4"))
