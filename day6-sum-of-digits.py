def sum_digits(num : int) -> int:
    sum=0
    while num > 0:
        n = num % 10
        sum+=n
        num//=10
    if sum>9:
        return sum_digits(sum)
    else:
        return sum
print(sum_digits(9786346233))
