#no zeros
# 3 sized meter
# strict ascending order
MAXIMUM = "789"
MINIMUM = "123"
size = 3

def strict_ascending(string : str) -> bool :
    for i in range(len(string)-1):
        if all(string[i] > string[i+1]):
            return True
        
def valid_reading(reading: str) -> bool:   
    if strict_ascending(reading):
        if( all(int(i)!=0) for i in reading) :
                return True


def next_readings(cur_reading : str) -> str:
    next = int(cur_reading) + 1
    next_reading = str(next)
    if len(next_reading) == size:
        if valid_reading(next_reading):
            return next_reading
        else:
            return next_readings(next_reading)
    else:
        return MINIMUM

def prev_readings(cur_reading : str) -> str:
    prev = int(cur_reading) - 1
    prev_reading = str(prev)
    if len(prev_reading) == size:
        if valid_reading(prev_reading):
            return prev_reading
        else:
            return prev_readings(prev_reading)
    else:
        return MAXIMUM
    
def step_next_reading(reading : int , step_count = int) -> str:
    count = 0
    while count!=step_count:
        next_reading =  reading + 1
        step_reading = str(next_reading)
        if valid_reading(step_reading) and len(step_reading) == size:
            count+=1
        else:
            count+=0
    return step_reading



print(step_next_reading(234 , 13))



