#no zeros
# 3 sized meter
# strict ascending order
MAXIMUM = "789"
MINIMUM = "123"
size = 3

def strict_ascending(reading : str) -> bool :
    return all(reading[i] < reading[i+1] for i in range(len(reading) - 1))

        
def valid_reading(reading: str) -> bool:   
    if strict_ascending(reading):
        if( all(int(i)!=0) for i in reading) :
            if len(reading)==size:
                return True


def next_readings(cur_reading : str) -> str:
    next = int(cur_reading) + 1
    next_reading = str(next)
    if len(next_reading) == size:
        if valid_reading(next_reading):
            return next_reading
        else:
            return next_readings(next_reading)
    else:
        return MINIMUM

def prev_readings(cur_reading : str) -> str:
    prev = int(cur_reading) - 1
    prev_reading = str(prev)
    if len(prev_reading) == size:
        if valid_reading(prev_reading):
            return prev_reading
        else:
            return prev_readings(prev_reading)
    else:
        return MAXIMUM
    
def step_next_reading(reading : int , step_count = int) -> int:
    count = 1
    next_reading = reading
    while count!= step_count:
        next_reading+= 1
        if valid_reading(str(next_reading)) :
            count+= 1
    return next_reading
        
def step_prev_reading(reading : int , step_count = int) -> int:
    count = 0
    prev_reading = reading
    while count!= step_count:
        prev_reading-= 1
        if valid_reading(str(prev_reading)) :
            count+= 1
    return prev_reading
        
def distance(reading_1 : int , reading_2 : int) -> int:
    count = 0
    next_reading = reading_1
    while next_reading != reading_2:
        next_reading+= 1
        if valid_reading(str(next_reading)) :
            count+= 1
    return count

print(step_prev_reading(245,2))

