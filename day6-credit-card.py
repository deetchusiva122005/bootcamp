def credit_card_check(number: int) -> int:
    total = ""
    card_number = str(number)
    for i in card_number[: : 2]:
        total+=i
    for i in card_number[1: :2]:
        total+= str(int(i) * 2)
    return check_digit(total)

def check_digit(total_number : str) -> int:
    sum = 0
    total = int(total_number)
    while total > 0:
        n = total % 10
        sum+= n
        total//=10
    return 10 -(sum % 10)

credit_card_check(401288888888188)
