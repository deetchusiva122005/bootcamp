def isbn(num : int) -> int:
    target_for_check = 0
    code_string = str(num)
    digits_odd = code_string[: len(code_string)-2: 2]
    digits_even = code_string[1:len(code_string)-2 : 2]

    for i in digits_even:
        target_for_check+= int(i)*3

    for j in digits_odd:
        target_for_check+= int(j)

    digit_for_check = target_for_check % 10
    return check_digit(digit_for_check , code_string)

def check_digit(digit_for_check : int , code_string : str) -> bool:
    if digit_for_check == 0:
        return code_string[-1] == 0
    else:
        return  (10-digit_for_check )== int(code_string[-1])
    
print(isbn(9781861973712))
