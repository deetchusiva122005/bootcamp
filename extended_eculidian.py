def euclidian_gcd(num1, num2):
    if num1  == 0:
        return num2, 0, 1

    gcd, a, b = extended_gcd(num2 % num1, num1)

    x = b - (num2 // num1) * a
    y = a

    return gcd, x, y


print(extended_gcd(78,21))
