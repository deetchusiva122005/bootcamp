DIGITS = '123456789'

def min_max(size: int) -> tuple[int, int]:
    return int(DIGITS[:size]), int(DIGITS[-size:])

class Odometer:
    def __init__(self, size: int) -> None:
        self._size = size
        self._min, self._max = min_max(size)
        self._reading = self._min

    def reading(self) -> int:
        return self._reading

    def __str__(self) -> str:
        return f'<{self._reading}>'

    def __repr__(self) -> str:
        size = len(str(self._reading))
        return f'Size: {size}, Reading: {self._reading}'

    def __eq__(self, other) -> bool:
        if self._size != other._size:
            raise ValueError(f"Incomparable Odometers: sizes {self._size}, {other._size}")
        return self._reading == other._reading

    def __lt__(self, other) -> bool:
        if self._size != other._size:
            raise ValueError("Odometers of different sizes are incomparable")
        return self._reading < other._reading

    @classmethod
    def is_ascending(cls, reading) -> bool:
        return all(a < b for a, b in zip(str(reading), str(reading)[1:]))

    def forward(self, step:int=1) -> None:
        for _ in range(step):
            if self._reading == self._max:
                self._reading = self._min
            else:
                self._reading += 1
                while not Odometer.is_ascending(self._reading):
                    self._reading += 1

    def backward(self,step:int=1) -> None:
        for _ in range(step):
            if self._reading == self._min:
                self._reading = self._max
            else:
                self._reading -= 1
                while not Odometer.is_ascending(self._reading):
                    self._reading -= 1

    def steps_for_forward(self,step_count : int) :
        count = 0
        while count!=step_count:
            if self._reading == self._max:
                self._reading = self._min
                count=1
            if step_count == count:
                break
            self._reading+=1
            while not Odometer.is_ascending(self._reading):
                    self._reading += 1
            count+=1

    def steps_for_backward(self,step_count : int) :
        count = 0
        while count!=step_count:
            if self._reading == self._min:
                self._reading = self._max
                count=1
            if step_count == count:
                break
            self._reading-=1
            while not Odometer.is_ascending(self._reading):
                    self._reading -= 1
            count+=1

    def distance(self , target : int):
        ini_reading = self._reading
        count = 0
        while ini_reading!= target :
            if ini_reading == self._max:
                ini_reading = self._min
                count+=1
            ini_reading+=1
            while not Odometer.is_ascending(ini_reading):
                    ini_reading += 1
            count+=1
        return count


o1=Odometer(3)
o1._reading=123
o1.steps_for_backward(1)
print(o1)
