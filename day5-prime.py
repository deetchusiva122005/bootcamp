import math
def prime_numbers( num : int) :
    primes=[]
    for i in  range(2,num+1):
        if i<10:
            if i==2 or  i==3:
                primes.append(i)
            elif i%2 !=0 and i%3 !=0:
                primes.append(i)
        else:
            if all((i%j)!=0 for j in range(2,10)):
                primes.append(i)
    return primes


print(prime_numbers(100))
