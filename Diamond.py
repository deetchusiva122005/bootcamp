import random
A = 1
J = 11
Q = 12
K = 13
heart = [1,2,3,4,5,6,7,8,9,10,11,12,13]
spade = [1,2,3,4,5,6,7,8,9,10,11,12,13]
clover = [1,2,3,4,5,6,7,8,9,10,11,12,13]
cards =[heart,spade,clover]

Diamond= [1,2,3,4,5,6,7,8,9,10,11,12,13]
def judge():
    chosen_dia = random.choice(Diamond)
    Diamond.remove(chosen_dia)
    return int(chosen_dia)

def comp_strategy(chosen_dia):
    if chosen_dia % 2 ==0:
        comp_bid = chosen_dia - 1
        return comp_bid

    if chosen_dia % 2 ==1 and chosen_dia != 13:
        comp_bid = chosen_dia + 1
        return comp_bid

    if chosen_dia == 13:
        comp_bid = chosen_dia
        return comp_bid
    
player_score = []
comp_score = []
def play_game(player_bid  ,  comp_bid , chosen_dia):
    if comp_bid > player_bid :
        comp_score.append(chosen_dia)

    elif player_bid > comp_bid: 
        player_score.append(chosen_dia)

    else:
        player_score.append(chosen_dia / 2)
        comp_score.append(chosen_dia / 2)

    return sum(player_score),sum(comp_score)



def game_conclusion(player_points , comp_points) :
    if comp_points > player_points:
        return "Computer won"

    elif player_points > comp_points :
        return "Player Won"

    else:
        return "Match Draw"


player_cards = input("Suit you wish ( Heart/Spade/Clover) : ")
print("Here A=1 , K=13 , Q=12, J=11 ")
print("\n")
def game():
    for i in range(1,14):
        chosen_dia = judge()
        print("The card for bid : " ,chosen_dia)
        player_bid = int(input("Card you bid : "))
        comp_bid = comp_strategy(chosen_dia)
        print("Card computer bid : ",comp_bid)
        print("\n")
        player_points , computer_points = play_game(player_bid ,  comp_bid , chosen_dia)
    print("player_points = ", player_points,"computer_points = ",computer_points)    
    return game_conclusion(player_points , computer_points)
print(game())

print(game())
