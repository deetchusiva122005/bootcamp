from collections import Counter

data = open("/content/mono_ciph014.txt").read()
freq = Counter(data)
freq.most_common(5)
word_freq = Counter(data.split())
word_freq.most_common(5)

data1 = data.replace('u','E').replace('i','H').replace('l','T').replace('p','A').replace('a','O').replace('0','I').replace('n','L').replace('o','N').replace('5','M')
data2 = data1.replace('"','C').replace("2",'R').replace('7','D').replace('b','F').replace('s','V').replace(';','S').replace('x','B').replace("f","Y").replace('8','U')
data3 = data2.replace("&","G").replace('8','U').replace('x','B').replace(',','K').replace('v','P').replace('f','Y').replace('v','P').replace('e','W').replace('z',',')
data4 = data3.replace('9','').replace('y','X').replace('h','Q').replace('g','.').replace('r','"').replace('k','?').replace('!',',')
print(data4)



from collections import Counter

data = open("/content/mono_ciph034.txt").read()
freq = Counter(data)
freq.most_common(5)
word_freq = Counter(data.split())
word_freq.most_common(5)

data1 = data.replace('c','T').replace('p','H').replace("'",'E').replace('7','R').replace('m','F').replace('9','N').replace('f','C')
data2 = data1.replace('s','O').replace('b','M').replace('*','B').replace('8','A').replace('w','S').replace('6','P').replace('u','W')
data3 = data2.replace('t','D').replace('x','U').replace('a','V').replace('h','I').replace('?','G').replace('r','X').replace('z','Y')
data4 = data3.replace('4','L').replace('-','K').replace(',','.').replace('!',',').replace('g','"').replace('@','').replace('d','?')
data5 = data4.replace('q','Q').replace('0','J').replace('1','!').replace('-',',').replace('-','!')
print(data5)



from collections import Counter

data = open("/content/mono_ciph054.txt").read()
freq = Counter(data)
freq.most_common(5)
word_freq = Counter(data.split())
word_freq.most_common(5)

data1 = data.replace('h','E').replace('d','H').replace('z','T').replace('5','I').replace('t','O')
data2 = data1.replace("'",'A').replace('m','B').replace('?','N').replace(',','P').replace('9','S')
data3 = data2.replace('.','V').replace('-','F').replace('u','R').replace('0','L').replace('k','D')
data4 = data3.replace('1','W').replace('7','C').replace(';','K').replace('3','U').replace('"','G')
data5 = data4.replace('!','Y').replace('e','M').replace('4','Q').replace('n',',').replace('&','"')
data6 = data5.replace('a','.').replace('*','!').replace('o','X').replace('p','.')
print(data6)
