def jail_problem(limit: int) -> list :
    doors_opened = []
    for i in range(1,int(limit**0.5)+1):
        doors_opened.append(i*i) 
    return(doors_opened)

print(jail_problem(100))
