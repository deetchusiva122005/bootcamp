import math
def sieve_of_eratosthenes(ini: int, limit: int) -> list:
    primes = [True] * (limit - ini + 1)
    primes[0], primes[1] = False, False

    for num in range(2, int(math.sqrt(limit)) + 1):
        if primes[num]:
            for multiple in range(num*num, limit + 1, num):
                if multiple >= ini:
                    primes[multiple - ini] = False

    return [num + ini for num in range(len(primes)) if primes[num]]
sieve_of_eratosthenes(100,1000)
