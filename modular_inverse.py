def modular_inverse(mod : int, inv : int):
    r1, r2 = mod, inv
    x1, y1 = 1, 0
    x2, y2 = 0, 1
    while r1 != 1 and r2 != 1:
        q = r1 // r2
        r1, r2 = r2, r1 - (r2 * q)
        x1, x2 = x2, x1 - (x2 * q)
        y1, y2 = y2, y1 - (y2 * q)
        
    return (inv, y2 % mod)

print(modular_inverse(186,13))
